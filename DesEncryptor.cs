﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesEncryption
{
    public class DesEncryptor : IDesEncryptor
    {
        protected int[] _initialPermutationTable = {
            58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4,
            62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8,
            57, 49, 41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3,
            61, 53, 45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7
        };

        private int[] _cKeyPermutationTable =
        {
            57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18,
            10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36
        };

        private int[] _dKeyPermutationTable =
        {
            63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22,
            14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4
        };

        private int[] _cdConcatTable =
        {
            14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4,
            26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40,
            51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32
        };

        private int[] _feistelExtensionTable =
        {
            32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9, 8, 9, 10, 11,
            12, 13, 12, 13, 14, 15, 16, 17, 16, 17, 18, 19, 20, 21, 20, 21,
            22, 23, 24, 25, 24, 25, 26, 27, 28, 29, 28, 29, 30, 31, 32, 1
        };

        private byte[,,] _sPermutationTables = {
            {
                { 14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7 },
                { 0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8 },
                { 4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0 },
                { 15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13 }
            },
            {
                {15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10 },
                {3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5 },
                {0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15 },
                {13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9 }
            },
            {
                { 10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8 },
                { 13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1 },
                { 13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7 },
                { 1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12 }
            },
            {
                { 7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15 },
                { 13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9 },
                { 10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4 },
                { 3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14 }
            },
            {
                { 2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9 },
                { 14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6 },
                { 4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14 },
                { 11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3 }
            },
            {
                { 12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11 },
                { 10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8 },
                { 9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6 },
                { 4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13 }
            },
            {
                { 4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1 },
                { 13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6 },
                { 1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2 },
                { 6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12 }
            },
            {
                { 13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7 },
                { 1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2 },
                { 7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8 },
                { 2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11 }
            }
        };

        private int[] _feistelFinalTable =
        {
            16, 7, 20, 21, 29, 12, 28, 17, 1, 15, 23, 26, 5, 18, 31, 10,
            2, 8, 24, 14, 32, 27, 3, 9, 19, 13, 30, 6, 22, 11, 4, 25
        };

        protected int[] _finalPermutationTable =
        {
            40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 47, 15, 55, 23, 63, 31,
            38, 6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 61, 29,
            36, 4, 44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27,
            34, 2, 42, 10, 50, 18, 58, 26, 33, 1, 41, 9, 49, 17, 57, 25
        };


        public string Encrypt(string str, string key)
        {
            if (key.Length != 8)
                throw new ArgumentException("Ключ должен состоять из 8 символов");

            // Если длина строки не позволяет разделить её на равные блоки по 8 символов, 
            // то в конец добавляется недостающее число этих символов
            if (str.Length % 8 > 0)
                str += new string('_', 8 - str.Length % 8);

            // Перевод строки в биты
            byte[] bits = str.ToBitArray();

            // Разделение массива всех битов на блоки
            byte[][] blocks = bits.Split(64);

            // Выполнение начальной перестановки IP(T) над каждым из 64 битных блоков
            for (int i = 0; i < blocks.Length; i++)
                blocks[i] = blocks[i].RearrangeArrayByTable(_initialPermutationTable);

            // Полученный после начальной перестановки 64-битовый блок IP(T) участвует в 16-циклах преобразования Фейстеля.
            for (int i = 0; i < blocks.Length; i++)
            {
                blocks[i] = EncryptBlock(blocks[i], key);              
            }

            // Конечная (обратная) перестановка
            for (int i = 0; i<blocks.Length;i++)
                blocks[i] = blocks[i].RearrangeArrayByTable(_finalPermutationTable);

            // Преобразование массива битов в массив байтов, затем массив байтов преобразовывается в строку
            return blocks.To64BaseStr();
        }

        public string Decrypt(string str, string key)
        {
            if (key.Length != 8)
                throw new ArgumentException("Ключ должен состоять из 8 символов");

            if (Convert.FromBase64String(str).Length % 8 != 0)
                throw new Exception("Строка не является зашированной");

            // Преобразование строки в биты
            byte[] bits = Convert.FromBase64String(str).ToBitArray();

            // Разделение массива всех битов на блоки по 64 бита
            byte[][] blocks = bits.Split(64);

            // Выполнение конечной перестановки над каждым из 64 битных блоков
            for (int i = 0; i < blocks.Length; i++)
                blocks[i] = blocks[i].RearrangeArrayByTable(_initialPermutationTable);

            for(int i = 0; i<blocks.Length;i++)
            {
                blocks[i] = DecryptBlock(blocks[i], key);
            }

            for (int i = 0; i < blocks.Length; i++)
                blocks[i] = blocks[i].RearrangeArrayByTable(_finalPermutationTable);

            return new string(blocks.SelectMany(i => i).ToArray().Split(8).Select(i => new BitValue(i).ToChar()).ToArray()).TrimEnd('_');

        }

        /// <summary>
        /// Зашифровать 64 битный блок используя ключ
        /// </summary>
        /// <param name="block"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        protected byte[] EncryptBlock(byte[] block,string key)
        {
            // Разделение 64 битного блока на субблоки по 32 бита
            byte[] L = block.Split(32).First(); // Старшие биты
            byte[] R = block.Split(32).Last(); // Младшие биты
            byte[] temp;

            for (int round = 1; round <= 16; round++)
            {
                temp = L;
                L = R; // L_i = R_(i-1) 
                R = temp.Xor(FeistelFunc(R, GenerateKey(key, round))); // R_i = L_(i-1) ^ f(R_(i-1),K_i)
            }

            return L.Concat(R).ToArray();
        }

        /// <summary>
        /// Расшифровать 64 битный блок используя ключ
        /// </summary>
        /// <param name="block"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        protected byte[] DecryptBlock(byte[] block, string key)
        {
            byte[] L = block.Split(32)[0];
            byte[] R = block.Split(32)[1];
            byte[] temp;

            for (int round = 16; round >= 1; round--)
            {
                temp = R;
                R = L;
                L = temp.Xor(FeistelFunc(L, GenerateKey(key, round)));
            }

            return L.Concat(R).ToArray();
        }

        private byte[] FeistelFunc(byte[] b, byte[] key)
        {
            return b.RearrangeArrayByTable(_feistelExtensionTable).Xor(key).Split(6)
                .Select((e, i) => STablePermutation(e, i))
                .SelectMany(i => i).ToArray().RearrangeArrayByTable(_feistelFinalTable);

        }

        /// <summary>
        /// Перестановка 8 битных блоков в соответствии с таблицей
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="tableNum"></param>
        /// <returns></returns>
        private byte[] STablePermutation(byte[] arr, int tableNum)
        {
            int rowNum = new BitValue(new byte[] { arr.First(), arr.Last() }).ToInt32();
            int colNum = new BitValue(arr.Slice(1, 4)).ToInt32();
            return new BitValue(_sPermutationTables[tableNum, rowNum, colNum]).ToArray();
        }

        /// <summary>
        /// Возвращает 48 битный расширенный ключ для определенного раунда
        /// </summary>
        /// <param name="key">Ключ шифрования</param>
        /// <param name="round"></param>
        /// <returns></returns>
        private byte[] GenerateKey(string key, int round)
        {
            if (key.Length != 8)
                throw new ArgumentException("Ключ шиврования должен состоять из 8 символов");

            byte[] bits = key.ToBitArray();

            byte[] C = bits.RearrangeArrayByTable(_cKeyPermutationTable);
            byte[] D = bits.RearrangeArrayByTable(_dKeyPermutationTable);


            for (int i = 1; i <= round; i++)
                if (new int[] { 1, 2, 9, 16 }.Contains(i))
                {
                    C = C.RotateLeft(1);
                    D = D.RotateLeft(1);
                }
                else
                {
                    C = C.RotateLeft(2);
                    D = D.RotateLeft(2);
                }

            return C.Concat(D).ToArray().RearrangeArrayByTable(_cdConcatTable);
        }
        


        #region HELPER METHODS

        #endregion


    }
}
