﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesEncryption
{
    public class DesEncryptorCFB : DesEncryptor, IDesEncryptor
    {
        private const int _k = 8;


        /*
        C[i] = P[i] XOR Fk(E(I[i]))
        I[i] = Sk(I[i - 1], C[i - 1])
        I[1] = IV

              IV
               |
             I[1]------Sk()--I[2]------Sk()--I[3] 
               |       /       |       /       |
              E()     /       E()     /       E()
               |     /         |     /         |
             Fk()   /        Fk()   /        Fk()
               |   /           |   /           | 
        P[1]--XOR /     P[2]--XOR /     P[3]--XOR
               | /             | /             | 
             C[1] C[2]            C[3]
        */


        public new string Encrypt(string str, string key)
        {
            if (key.Length != 8)
                throw new ArgumentException("Ключ должен состоять из 8 символов");

            if (str.Length % 8 > 0)
                str += new string('_', 8 - str.Length % 8);

            // Перевод строки в биты
            byte[][] blocks = str.ToBitArray().Split(_k);

            byte[] I = new RandomIntSequenceProvider(5, 2, 3, 1).GenerateSequence(64).Select(i => (byte)i).ToArray();

            byte[][] C = new byte[blocks.Length][];


            for (int i = 0; i < blocks.Length; i++)
            {
                C[i] = blocks[i].Xor(this.EncryptBlock(I, key).Take(_k).ToArray());
                I = I.ShitLeft(_k, C[i]);
            }

            return C.To64BaseStr();
        }

        // P[i] = C[i] xor Fk(E(I[i]))
        // I[i] = shift(I[i-1],C[i-1])

        public new string Decrypt(string str, string key)
        {
            if (key.Length != 8)
                throw new ArgumentException("Ключ должен состоять из 8 символов");

            if (Convert.FromBase64String(str).Length % 8 != 0)
                throw new Exception("Строка не является зашированной");

            // Преобразование строки в биты
            byte[] bits = Convert.FromBase64String(str).ToBitArray();

            // Разделение массива всех битов на блоки по 64 бита
            byte[][] blocks = bits.Split(_k);

            byte[] I = new RandomIntSequenceProvider(5, 2, 3, 1).GenerateSequence(64).Select(i => (byte)i).ToArray();

            byte[][] P = new byte[blocks.Length][];

            for (int i = 0; i < blocks.Length; i++)
            {
                P[i] = blocks[i].Xor(EncryptBlock(I, key).Take(8).ToArray());
                I = I.ShitLeft(8, blocks[i]);
            }

            return new string(P.SelectMany(i => i).ToArray().Split(8).Select(i => new BitValue(i).ToChar()).ToArray()).TrimEnd('_');
        }
    }
}
