﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesEncryption
{
    public class DesEncryptorCBC : DesEncryptor, IDesEncryptor
    {
        public new string Encrypt(string str, string key)
        {
            if (key.Length != 8)
                throw new ArgumentException("Ключ должен состоять из 8 символов");

            if (str.Length % 8 > 0)
                str += new string('_', 8 - str.Length % 8);

            byte[] bits = str.ToBitArray();

            byte[][] blocks = bits.Split(64);

            for (int i = 0; i < blocks.Length; i++)
                blocks[i] = blocks[i].RearrangeArrayByTable(_initialPermutationTable);

            // Генерирование начальной псевдо случайной последовательности
            byte[] c = new RandomIntSequenceProvider(5, 2, 3, 1).GenerateSequence(64).Select(i=>(byte)i).ToArray();

            for(int i = 0; i<blocks.Length;i++)
            {
                blocks[i] = EncryptBlock(blocks[i].Xor(c), key);
                c = blocks[i];
            }

            for (int i = 0; i < blocks.Length; i++)
                blocks[i] = blocks[i].RearrangeArrayByTable(_finalPermutationTable);

            return blocks.To64BaseStr();
        }

        public new string Decrypt(string str, string key)
        {
            if (key.Length != 8)
                throw new ArgumentException("Ключ должен состоять из 8 символов");

            if (Convert.FromBase64String(str).Length % 8 != 0)
                throw new Exception("Строка не является зашированной");

            byte[] bits = Convert.FromBase64String(str).ToBitArray();

            byte[][] blocks = bits.Split(64);

            for (int i = 0; i < blocks.Length; i++)
                blocks[i] = blocks[i].RearrangeArrayByTable(_initialPermutationTable);

            // Генерирование начальной псевдо случайной последовательности
            byte[] c = new RandomIntSequenceProvider(5, 2, 3, 1).GenerateSequence(64).Select(i => (byte)i).ToArray();
            byte[] temp;

            for (int i = 0; i < blocks.Length; i++)
            {
                temp = blocks[i];
                blocks[i] = c.Xor(DecryptBlock(blocks[i], key));
                c = temp;
            }

            for (int i = 0; i < blocks.Length; i++)
                blocks[i] = blocks[i].RearrangeArrayByTable(_finalPermutationTable);

            return new string(blocks.SelectMany(i => i).ToArray().Split(8).Select(i => new BitValue(i).ToChar()).ToArray()).TrimEnd('_');
        }
    }
}
