# DES Encryption #

### C# Implementation of DES Encryption (ECB, CBC, CFB, OFB modes) + DotNet DES (ECB) Implementation ###
-----
**Реализция криптографического алгоритма DES (Data Encryption Standard) в режимах ECB, CBC, CFB, OFB**


* ECB (Electronic Code Book), электронный кодоблокнот;
* CBC (CipherBlock Chaining), сцепление блоков шифра;
* CFB (Cipher FeedBack), обратная связь по шифртексту;
* OFB (Output FeedBack), обратная связь по выходу.

Алгоритм DES - https://ru.wikipedia.org/wiki/DES


***Пример:***

```
#!c#

string key = "12345678";
string plainText = "my simple text";

// ECB mode
IDesEncryptor encryptor = new DesEncryptor();
string encryptedText = encryptor.Encrypt(plainText, key); 
string decryptedText = encryptor.Decrypt(encryptedText, key); 

// CBC mode
IDesEncryptor encryptor = new DesEncryptorCBC();
string encryptedText = encryptor.Encrypt(plainText, key); 
string decryptedText = encryptor.Decrypt(encryptedText, key); 

...

```
---
Скачать демо | Download Demo - [Link](https://www.dropbox.com/s/dklgk75v4vthtl0/DesEncryption.exe?dl=0)

Скачать исходники | Download Source Code - [Link](https://www.dropbox.com/s/g7og9mqwzl2mx7j/DES%20Encryption%20Source%20Code.rar?dl=0)

![Безымянный.png](https://bitbucket.org/repo/bbGae7/images/3466351938-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.png)