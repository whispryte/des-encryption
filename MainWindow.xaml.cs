﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DesEncryption
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Dictionary<string, IDesEncryptor> _encryptorsList = new Dictionary<string, IDesEncryptor>()
        {
            ["ecb"] = new DesEncryptor(),
            ["cbc"] = new DesEncryptorCBC(),
            ["cfb"] = new DesEncryptorCFB(),
            ["ofb"] = new DesEncryptorOFB()
        };

        public MainWindow()
        {
            InitializeComponent();


            var a = new DesEncryptor().Encrypt("aaa", "12345678");
            var b = new DesEncryptorCBC().Encrypt("aaa", "12345678");
            
        }

        private void EncryptButtonClick(object sender, RoutedEventArgs e)
        {
            string str = textToEncrypt.Text;
            string key = encryptionKey.Text;

            if (str.Length == 0)
            {
                encryptedText.Text = "";
                return;
            }

            if(key.Length!=8)
            {
                MessageBox.Show("Ключ должен состоять из 8 символов");
                return;
            }

            string mode = (encryptionModeComboBox.SelectedItem as ComboBoxItem).Name;
            var encryptor = _encryptorsList[mode];
            encryptedText.Text = encryptor.Encrypt(str, key);
            encryptedBytes.Text = string.Join(" ", Convert.FromBase64String(encryptedText.Text));
        }

        private void DecryptButtonClick(object sender, RoutedEventArgs e)
        {
            string str = textToDecrypt.Text;
            string key = encryptionKey.Text;

            if (str.Length == 0)
            {
                decryptedText.Text = "";
                return;
            }

            if (key.Length != 8)
            {
                MessageBox.Show("Ключ должен состоять из 8 символов");
                return;
            }


            string mode = (encryptionModeComboBox.SelectedItem as ComboBoxItem).Name;
            var encryptor = _encryptorsList[mode];
            this.decryptedText.Text = encryptor.Decrypt(str, key);
            this.decryptedBytes.Text = string.Join(" ", Encoding.ASCII.GetBytes(decryptedText.Text));
        }


    }
}
