﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesEncryption
{
    public class DesEncryptorOFB : DesEncryptor, IDesEncryptor
    {
        // Количество битов в блоке
        private const int _k = 8;


        /*
        C[i] = P[i] xor F[i]
        F[i] = Fk(E(I[i]))
        I[i] = Shift(I[i-1], F[i-1])
        */
        public new string Encrypt(string str, string key)
        {
            if (key.Length != 8)
                throw new ArgumentException("Ключ должен состоять из 8 символов");

            if (str.Length % 8 > 0)
                str += new string('_', 8 - str.Length % 8);

            byte[][] blocks = str.ToBitArray().Split(_k);
            byte[] I = new RandomIntSequenceProvider(5, 2, 3, 1).GenerateSequence(64).Select(i => (byte)i).ToArray();
            byte[][] C = new byte[blocks.Length][];
            byte[] F;

            for(int i = 0; i<blocks.Length;i++)
            {
                F = EncryptBlock(I, key).Take(_k).ToArray();
                C[i] = blocks[i].Xor(F);
                I = I.ShitLeft(_k,F);
            }

            return C.To64BaseStr();

        }

        /*
        P[i] = C[i] xor F[i]
        F[i] = Fk(E(I[i]))
        I[i] = Shift(I[i-1],F[i-1])
        */
        public new string Decrypt(string str, string key)
        {
            if (key.Length != 8)
                throw new ArgumentException("Ключ должен состоять из 8 символов");

            if (Convert.FromBase64String(str).Length % 8 != 0)
                throw new Exception("Строка не является зашированной");

            // Преобразование строки в биты
            byte[] bits = Convert.FromBase64String(str).ToBitArray();

            // Разделение массива всех битов на блоки по 64 бита
            byte[][] blocks = bits.Split(_k);

            byte[] I = new RandomIntSequenceProvider(5, 2, 3, 1).GenerateSequence(64).Select(i => (byte)i).ToArray();

            byte[][] P = new byte[blocks.Length][];
            byte[] F;


            for(int i = 0; i<blocks.Length;i++)
            {
                F = EncryptBlock(I, key).Take(_k).ToArray();
                P[i] = blocks[i].Xor(F);
                I = I.ShitLeft(_k, F);
            }


            return new string(P.SelectMany(i => i).ToArray().Split(8).Select(i => new BitValue(i).ToChar()).ToArray()).TrimEnd('_');
        }
    }
}
