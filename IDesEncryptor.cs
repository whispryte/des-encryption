﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesEncryption
{
    public interface IDesEncryptor
    {
        /// <summary>
        /// Зашифровать строку используя ключ
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key">Строка из 8 символов</param>
        /// <returns></returns>
        string Encrypt(string str, string key);

        /// <summary>
        /// Расшифровать строку используя ключ
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key">Строка из 8 символов</param>
        /// <returns></returns>
        string Decrypt(string str, string key);
    }
}
