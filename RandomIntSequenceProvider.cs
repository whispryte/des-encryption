﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesEncryption
{
    public class RandomIntSequenceProvider
    {
        private readonly int _a, _b, _c, _t0;

        /// <summary>
        /// Создание последовательности псевдослучайных чисел
        /// </summary>
        /// <param name="a">Константа</param>
        /// <param name="b">Длина алфавита</param>
        /// <param name="c">Константа</param>
        /// <param name="t0">Пораждающее значение</param>
        public RandomIntSequenceProvider(int a, int b, int c, int t0)
        {
            if (b < 2)
                throw new ArgumentException("Алфавит последовательности не может состоять меньше чем из двух символов");
            if (t0 >= b)
                throw new ArgumentException("Пораждающее значение не входит в алфавит");

            _a = a;
            _c = c;
            _t0 = t0;
            _b = b;            
        }

        /// <summary>
        /// Генерирование псевдослучайной последовательности заданной длины
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public IEnumerable<int> GenerateSequence(int length)
        {
            List<int> list = new List<int>();
            list.Add(_t0);
            for(int i = 1; i<length;i++)
            {
                list.Add((_a * list[i - 1] + _c) % _b);
            }
            return list;
        }
    }
}
