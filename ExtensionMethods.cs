﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesEncryption
{
    public static class ExtensionMethods
    {

        /// <summary>
        /// Разделяет массив на массивы заданной длины
        /// </summary>
        /// <param name="array"></param>
        /// <param name="length">Длина подмассива</param>
        /// <returns></returns>
        public static byte[][] Split(this byte[] array, int length) =>
            array.Select((e,i)=> new { Index = i, Value = e }).GroupBy(i => i.Index/length).
            Select(g=>g.Select(e=>e.Value).ToArray()).ToArray();

      
        /// <summary>
        /// Перевод строки в битовый массив
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static byte[] ToBitArray(this string str) 
            => Encoding.ASCII.GetBytes(str).Select(i => new BitValue(i)).SelectMany(i => i).ToArray();

        /// <summary>
        /// Перевод массива байтов в массив битов
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static byte[] ToBitArray(this IEnumerable<byte> array)
            => array.Select(i => new BitValue(i)).SelectMany(i => i).ToArray();


        /// <summary>
        /// Усечение массива по начальному индексу и длине
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="index">Начальный индекс</param>
        /// <param name="length">Длина</param>
        /// <returns></returns>
        public static T[] Slice<T>(this T[] source, int index, int length)
        {
            if (index + length > source.Length || length<1)
                throw new ArgumentException("Не верно заданы индексы массива для усечения");

            T[] slice = new T[length];
            Array.Copy(source, index, slice, 0, length);
            return slice;
        }

        /// <summary>
        /// Преобразование входного массива с помощью таблицы перестановок
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="inputArray"></param>
        /// <param name="permutationTable">Таблица, значения которой - это индексы во входном массиве</param>
        /// <returns></returns>
        public static T[] RearrangeArrayByTable<T>(this T[] inputArray, int[] permutationTable)
        {
            if (permutationTable.Max() > inputArray.Length)
                throw new ArgumentException("Таблица не совместима с исходным массивом");
            return permutationTable.Select(i => inputArray[i-1]).ToArray();
        }

        /// <summary>
        /// Сдвигает элементы массива влево на заданное значение 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="arr"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public static T[] RotateLeft<T>(this T[] arr, int offset)
        {
            offset = offset % arr.Length;
            if (offset == 0)
                return arr;
            return arr.Skip(offset).Concat(arr.Take(offset)).ToArray();
        }

        /// <summary>
        /// Сдвигает элементы массива вправо на заданное значение
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="arr"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public static T[] RotateRight<T>(this T[] arr, int offset)
        {
            offset = offset % arr.Length;
            if (offset == 0)
                return arr;
            return arr.Skip(arr.Length - offset).Concat(arr.Take(arr.Length - offset)).ToArray();

        }


        /// <summary>
        /// Сложение массивов по модулю 2 (строгое ИЛИ)
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="arr2"></param>
        /// <returns></returns>
        public static byte[] Xor(this byte[] arr, byte[] arr2)
        {
            if (arr.Length != arr2.Length)
                throw new ArgumentException("Длины массивов должны совпадать");
            if (arr.Any(i => i != 1 && i != 0) || arr2.Any(i => i != 1 && i != 0))
                throw new ArgumentException("Массивы должны содежать только нули и единицы");

            return arr.Select((e, i) => (byte)(Math.Max(e, arr2[i]) - Math.Min(e, arr2[i]))).ToArray();            
        }

        /// <summary>
        /// Сдвигает массив влево на указанное количество элементов. Недостающие сиволы справа 
        /// заполняются из массива донора
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="offset">Величина сдвига</param>
        /// <param name="donorArray">Массив из которого будет произведено заполнение недостаюзих элементов</param>
        /// <returns></returns>
        public static T[] ShitLeft<T>(this T[] array, int offset, T[] donorArray)
        {
            if (array.Length <= offset)
                throw new ArgumentException("Невозможно сдвинуть массив на величину больше чем его длина или на равную ей");
            if (offset > donorArray.Length)
                throw new ArgumentException("Длина массива-донора не позволяет заполнить недостающие элементы");

            return array.Skip(offset).Concat(donorArray.Take(array.Length - offset)).ToArray();
        }

        /// <summary>
        /// Преобразовать двумерный массив битов в строку base64
        /// </summary>
        /// <param name="blocks"></param>
        /// <returns></returns>
        public static string To64BaseStr(this byte[][] blocks) =>
            Convert.ToBase64String(blocks.SelectMany(i => i).ToArray().Split(8).Select(i => new BitValue(i).ToByte()).ToArray());

    }
}
