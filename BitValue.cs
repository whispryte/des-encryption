﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesEncryption
{
    public class BitValue : IEnumerable<byte>
    {
        private List<byte> _bits = new List<byte>();

        public byte this[int key]
        {
            get { return _bits[key]; }
            set { _bits[key] = value; }
        }

        /// <summary>
        /// Возвращает количество битов
        /// </summary>
        public int Count => _bits.Count;


        /// <summary>
        /// Создает битовое значение из байта (путем конвертирования десятичного восьмиразрядное числа в двоичное)
        /// </summary>
        /// <param name="value"></param>
        /// <param name="bitNum">Разрядность значения</param>
        public BitValue(byte value, int bitNum = 8)
        {
            this.SetBits(Convert.ToString(value, 2).Select(i => byte.Parse(i.ToString())), bitNum);
        }

        /// <summary>
        /// Создает битовое значение из массива битов
        /// </summary>
        /// <param name="bitArray">Массив содержащий нули и единицы</param>
        /// <param name="bitNum">Разрядность значения</param>
        /// <exception cref="System.ArgumentException">Если в массиве присутствуют значения отличные от 0 и 1</exception>
        public BitValue(IEnumerable<byte> bitArray, int bitNum = 8)
        {
            if (bitArray.Any(i => i != 0 && i != 1))
                throw new ArgumentException("Массив должен состоять из нулей и единиц");
            this.SetBits(bitArray, bitNum);
        }

        /// <summary>
        /// Создает битовое значение из строки содержащей нули и единицы
        /// </summary>
        /// <param name="bitString">Битовая строка (01100010)</param>
        /// <exception cref="System.ArgumentException">Если в строке присутствуют значения отличные от 0 и 1</exception>
        public BitValue(string bitString, int bitNum = 8)
        {
            if (bitString.Any(i => i != '1' || i != '0'))
                throw new ArgumentException("Битовая строка должна содержать только единички и нули");
            else
            {
                this.SetBits(bitString.Select(i => byte.Parse(i.ToString())),bitNum);
            }
        }
        

        public IEnumerator<byte> GetEnumerator()
        {
            foreach (byte i in _bits)
                yield return i;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            return string.Join("", _bits);
        }

        public int ToInt32() => Convert.ToInt32(this.ToString(), 2);
        public byte ToByte() => Convert.ToByte(this.ToString(), 2);
        public char ToChar() => (char)ToInt32();
      
     

        private void SetBits(IEnumerable<byte> bits, int bitNum)
        {
            if (bits.Count() % bitNum != 0)
                _bits.AddRange(Enumerable.Repeat((byte)0, bitNum - bits.Count() % bitNum));
            _bits.AddRange(bits);
        }

    }
}
